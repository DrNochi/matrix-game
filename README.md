# Matrix

**This is a plain .NET Core port ( *I did not change a single line of code* ) of a small game I created serveral years (4+) ago in MS VC++. The original files can be found in `MatrixConsole.zip`.**

The game let's you type in a text and then you must search for it in a matrix-like console ("green rain").
The game has serveral settings that can be changed in the pause menu (pressing `p` in-game) so that the playing experience and difficulty can be adjusted.

## Playing

1. To play the game you need to have [`dotnet`](https://dot.net) installed.
2. Run the following command: `dotnet run`
3. Follow the instructions on screen

---
*This is old code and a very early project of mine. The software design is not very good and performance may be bad (on my linux system it's considerably slower than on Windows). **A issue has been opened to address these problems.***
