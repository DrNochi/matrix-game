﻿using System;
using System.Threading;

namespace Matrix
{
    class Program
    {
        static int neededTime = 0; // The time

        static void timerElapsed(Object source, System.Timers.ElapsedEventArgs e)
        {
            neededTime++;
        }

        static void Main(string[] args)
        {
            MatrixRain mr = new MatrixRain(); // An instance of MatrixRain

            System.Timers.Timer time = new System.Timers.Timer(1000); // A timer
            time.Elapsed += timerElapsed; // Registers the elapsed event handler
            time.AutoReset = true; // Sets the timer to auto reset

            int width, height; // The width and height of the console window
            int[] y; // The width-height array
            String txt; // The text

            Console.ForegroundColor = ConsoleColor.DarkGray; // Sets the forground color to dark gray

            try
            {
                Console.WindowLeft = 0; // Sets the window left to 0
                Console.WindowTop = 0; // Sets the window top to 0
                Console.WindowHeight = Console.LargestWindowHeight; // Sets the window height to max
                Console.BufferHeight = Console.LargestWindowHeight; // Sets the buffer height to max
                Console.WindowWidth = Console.LargestWindowWidth; // Sets the window width to max
                Console.BufferWidth = Console.LargestWindowWidth; // Sets the buffer width to max
            }
            catch (PlatformNotSupportedException e)
            {
                // Ignore
            }

            Console.CursorVisible = true; // Shows the cursor
            Console.Write("Type your Text: "); // Asks the user for the text
            txt = Console.ReadLine(); // Sets the text to the user input
            Console.CursorVisible = false; // Hides the cursor

            if (txt != "test")
            {
                Console.Write("Hit any key to continue!"); // Tell user to hit a key
                Console.ReadKey(); // Wait for a user input

                // Intro / Tutorial
                Console.Clear(); // Clears the console
                Console.Write("In Game: Press 'p' for [P]ause");
                Thread.Sleep(3000); // Wait for 3 sec

                Console.Clear(); // Clears the console
                Console.Write(" Search the text you typed in!");
                Thread.Sleep(3000); // Wait for 3 sec

                Console.Clear(); // Clears the console
                Console.Write("When you found the text hit 'f' to see the time you needed");
                Thread.Sleep(3000); // Wait for 3 sec
            }

            mr.Initialize(txt, out width, out height, out y); // Calls Initialize
            time.Start();

            while (true)
            {
                // Update screen
                mr.UpdateAllColumns(width, height, ref y);
                // Thread.Sleep(1);

                // Check if window has been resized
                if (Console.WindowWidth - 1 != width || Console.WindowHeight != height)
                    mr.UpdateWindow(out width, out height, out y); // Recalc for new size

                if (Console.KeyAvailable)
                    switch (Console.ReadKey().Key)
                    {
                        case ConsoleKey.P:
                            time.Stop(); // Stops the timer

                        // Shows the menu
                        MENU:
                            Console.SetCursorPosition(0, 0); // Sets the cursor position to 0, 0
                            Console.ForegroundColor = ConsoleColor.Yellow; // Sets the forground color to yellow
                            Console.WriteLine("Search: " + "\"" + txt + "\", Time: " + neededTime); // Writes the text
                            Console.Write("un[P]ause, [R]eset, [E]xit, new [T]ext, [N]ew text position, [C]olorize text \ntoggle [F]ancy colors, toggle random column [S]kipping \nchange [B]lanc factor, show [A]ll Settings \n- ");

                            while (true)
                            {
                                // Check if window has been resized
                                if (Console.WindowWidth - 1 != width || Console.WindowHeight != height)
                                    mr.UpdateWindow(out width, out height, out y); // Recalc for new size

                                if (Console.KeyAvailable)
                                    switch (Console.ReadKey().Key)
                                    {
                                        case ConsoleKey.P:
                                            goto BREAK_PAUSE; // Exists the loop

                                        case ConsoleKey.R:
                                            mr.Initialize(txt, out width, out height, out y); // Calls Initialize
                                            neededTime = 0; // Resets time

                                            // Shows the menu
                                            Console.WriteLine("Search: " + "\"" + txt + "\", Time: " + neededTime); // Writes the text
                                            Console.Write("un[P]ause, [R]eset, [E]xit, new [T]ext, [N]ew text position, [C]olorize text \ntoggle [F]ancy colors, toggle random column [S]kipping \nchange [B]lanc factor, show [A]ll Settings \n- ");
                                            break;

                                        case ConsoleKey.E:
                                            return; // Exit

                                        case ConsoleKey.T:
                                            Console.CursorVisible = true; // Shows the cursor
                                            Console.Write("\nNew Text: "); // Asks for new text
                                            txt = Console.ReadLine(); // Saves the user input
                                            Console.CursorVisible = false; // Hides the cursor

                                            mr.NewText(txt, width); // Set new text

                                            Console.Write("- "); // New line
                                            break;

                                        case ConsoleKey.N:
                                            mr.NewTxtPosition(width, height);
                                            break;

                                        case ConsoleKey.C:
                                            mr.ToggleColorize();
                                            break;

                                        case ConsoleKey.F:
                                            mr.ToggleFancyColor();
                                            break;

                                        case ConsoleKey.S:
                                            mr.ToggleColumnSkipping();
                                            break;

                                        case ConsoleKey.A:
                                            mr.DisplaySettings();
                                            break;

                                        case ConsoleKey.B:
                                            Console.CursorVisible = true; // Show cursor
                                            Console.Write("\nBlank-Factor: "); // Asks for the factor

                                            mr.SetBlankSpaceFactor(int.Parse(Console.ReadLine())); // Set factor

                                            Console.CursorVisible = false; // Hide cursor
                                            Console.Write("- "); // New line
                                            break;
                                    }
                            }
                        BREAK_PAUSE:
                            time.Start();
                            break;

                        case ConsoleKey.F:
                            time.Stop(); // Stop timer

                            // Display win message
                            Console.Clear(); // Clears the console
                            Console.SetCursorPosition(0, 0); // Sets the cursor position to 0, 0
                            Console.ForegroundColor = ConsoleColor.Yellow; // Sets the forground color to yellow
                            Console.WriteLine("Congrats! You found the text! \nYou needed " + neededTime + "sec.");
                            Console.ForegroundColor = ConsoleColor.DarkGray; // sets the console color to dark gray
                            Console.Write("Hit any key to continue!"); // Tell user to hit a key
                            Console.ReadKey(); // Wait for a user input

                            goto MENU;
                    }
            }
        }
    }
}
