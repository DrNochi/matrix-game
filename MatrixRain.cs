using System;

namespace Matrix
{
    enum TxtColor
    {
        RED,
        YELLOW,
        GREEN,
        CYAN,
        BLUE,
        MAGENTA
    }

    class MatrixRain
    {
        Random rand = new Random(); // Generator for random numbers

        string cmdTxt; // Text
        int cmdTxtX; // Text position (X)
        int cmdTxtY; // Text position (Y)

        // Random ASCII-character
        char AsciiCharacter
        {
            get
            {
                int t = rand.Next(10); // Generates a random number

                if (t <= 2) return (char)('0' + rand.Next(10)); // 0-9
                else if (t <= 4) return (char)('a' + rand.Next(27)); // Small letter
                else if (t <= 6) return (char)('A' + rand.Next(27)); // Capital letter
                else return (char)rand.Next(32, 255); // Any ASCII-char
            }
        }

        // Settings
        bool colorize = false; // Is text marked?
        bool fancyColor = true; // Are red and white ASCII-Character used?
        bool columnSkip = true; // Do skipping of random columns

        TxtColor textColor = TxtColor.RED;
        int fancyColorFactor = 1; // The factor for fancy colors
        int columnSkipFactor = 1; // The factor for column skipping
        int blankSpace = 1; // The factor for blank fields

        // Checks if the cursor position (Y) is in the console window
        int InScreenYPosition(int yPosition, int height)
        {
            if (yPosition < 0) return yPosition + height; // Returns wrapped cursor position (Y)
            else if (yPosition < height) return yPosition; // Return cursor position (Y)
            else return 0; // Returns 0
        }

        // Sets the width, height, the width-height array and the text
        public void Initialize(String txt, out int width, out int height, out int[] y)
        {
            width = Console.WindowWidth - 1; // Sets the width
            height = Console.WindowHeight; // Sets the height

            cmdTxt = txt; // Sets the text
            cmdTxtX = rand.Next(width + 1 - cmdTxt.Length); // Sets the text position (X)
            cmdTxtY = rand.Next(height); // Sets the text position (Y)

            y = new int[width]; // Defines the width-height array

            Console.Clear(); // Clears the console

            // Sets the members of the width-height array
            for (int i = 0; i < width; i++) y[i] = rand.Next(height);
        }

        // Writes a character to the i-th column
        void WriteColumn(int i, char c)
        {
            // Checks if current position is in the text region
            if (Console.CursorLeft >= cmdTxtX && Console.CursorLeft < cmdTxtX + cmdTxt.Length && Console.CursorTop == cmdTxtY)
            {
                // If colorize, sets the foreground color to the text color
                if (colorize)
                    switch (textColor)
                    {
                        case TxtColor.RED:
                            Console.ForegroundColor = ConsoleColor.Red;
                            break;

                        case TxtColor.YELLOW:
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            break;

                        case TxtColor.GREEN:
                            Console.ForegroundColor = ConsoleColor.Green;
                            break;

                        case TxtColor.CYAN:
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            break;

                        case TxtColor.BLUE:
                            Console.ForegroundColor = ConsoleColor.Blue;
                            break;

                        case TxtColor.MAGENTA:
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            break;
                    }

                Console.Write(cmdTxt[i - cmdTxtX]); // Writes one letter of the text
            }
            else Console.Write(c); // Writes a random ASCII-character
        }

        // Updates all columns of the console window
        public void UpdateAllColumns(int width, int height, ref int[] y)
        {
            int fancy; // Defines an integer

            if (fancyColor) fancy = rand.Next((int)(height * 4.48 / fancyColorFactor)); // Fancy is set to a random number
            else fancy = 121 / fancyColorFactor; // Fancy is set equal to 61

            for (int i = 0; i < width; i++)
            {
                Console.ForegroundColor = ConsoleColor.Green; // Set the console foregournd color to green
                Console.SetCursorPosition(i, y[i]); // Set the cursor position
                WriteColumn(i, AsciiCharacter); // Writes to the i-th column

                // Determine foreground color for tear drop effect
                if (i % (30 / fancyColorFactor) == fancy / (4 * fancyColorFactor))
                    Console.ForegroundColor = ConsoleColor.White;
                else if (i % (120 / fancyColorFactor) == fancy)
                    Console.ForegroundColor = ConsoleColor.Red;
                else
                    Console.ForegroundColor = ConsoleColor.DarkGreen;

                Console.SetCursorPosition(i, InScreenYPosition(y[i] - 2, height)); // Set the cursor position (tear drop)
                WriteColumn(i, AsciiCharacter); // Writes to the i-th column (tear drop)

                Console.SetCursorPosition(i, InScreenYPosition(y[i] - (int)((height * 0.8) / blankSpace), height)); // Sets the cursor position (blanks)
                WriteColumn(i, ' '); // Writes to the i-th column (blank)

                y[i] = InScreenYPosition(y[i] + 1, height); // Increases the saved cursor position (Y) by one

                if (columnSkip && i % (30 / columnSkipFactor) == rand.Next(121 / columnSkipFactor) / (4 * columnSkipFactor))
                    i += width / 8; // Skippes some lines
            }
        }

        // Updates the width, the height and the width-height array
        public void UpdateWindow(out int width, out int height, out int[] y)
        {
            width = Console.WindowWidth - 1; // Updates the width
            height = Console.WindowHeight; // Updates the height

            y = new int[width]; // Defines new width-height array

            // Sets the members of the width-height array
            for (int i = 0; i < width; i++) y[i] = rand.Next(height);

            // Recalc the text position (X)
            if (width + 1 - cmdTxtX < cmdTxt.Length) cmdTxtX -= cmdTxt.Length - width + 1 + cmdTxtX;

            // Recalc the text position (Y)
            if (!(cmdTxtY < height)) cmdTxtY -= cmdTxtY + 1 - height;
        }

        // Changes the text and if needed the text position
        public void NewText(String txt, int width)
        {
            cmdTxt = txt; // Changes the text

            // Recalc the text position (X)
            if (width + 1 - cmdTxtX < cmdTxt.Length) cmdTxtX -= cmdTxt.Length - width + 1 + cmdTxtX;
        }

        // Changes the text position
        public void NewTxtPosition(int width, int height)
        {
            cmdTxtX = rand.Next(width + 1 - cmdTxt.Length); // Generates new text position (x)
            cmdTxtY = rand.Next(height); // Generates new text position (Y)
        }

        // Toggles marking of the text
        public void ToggleColorize()
        {
            colorize = !colorize; // Toggle colorize

            if (colorize)
            {
                // Writes all the colors
                Console.Write("\nChoose Color: [R]ed, [Y]ellow, [G]reen, [C]yan, [B]lue, [M]agenta - ");
                ConsoleKey key = Console.ReadKey().Key; // Saves the user input

                switch (key)
                {
                    case ConsoleKey.R:
                        SetTextColor(TxtColor.RED);
                        break;

                    case ConsoleKey.Y:
                        SetTextColor(TxtColor.YELLOW);
                        break;

                    case ConsoleKey.G:
                        SetTextColor(TxtColor.GREEN);
                        break;

                    case ConsoleKey.C:
                        SetTextColor(TxtColor.CYAN);
                        break;

                    case ConsoleKey.B:
                        SetTextColor(TxtColor.BLUE);
                        break;

                    case ConsoleKey.M:
                        SetTextColor(TxtColor.MAGENTA);
                        break;
                }

                Console.Write("\n- "); // New line
            }
        }

        // Toggles usage of red and white characters
        public void ToggleFancyColor()
        {
            fancyColor = !fancyColor; // Toggles fancyColor

            if (fancyColor)
            {
                Console.CursorVisible = true; // Show cursor
                Console.Write("\n Fancy-Color-Factor: "); // Asks for Fancy-Color-Factor

                SetFancyColorFactor(int.Parse(Console.ReadLine())); // Sets the factor

                Console.CursorVisible = false; // Hide cursor
                Console.Write("- "); // New line
            }
        }

        // Sets the text color
        public void SetTextColor(TxtColor nTextColor)
        {
            textColor = nTextColor;
        }

        // Toggle the skipping of random columns
        public void ToggleColumnSkipping()
        {
            columnSkip = !columnSkip; // Toggles columnSkip

            if (columnSkip)
            {
                Console.CursorVisible = true; // Show cursor
                Console.Write("\n Column-Skip-Factor: "); // Asks for Column-Skip-Factor

                SetColumnSkipFactor(int.Parse(Console.ReadLine())); // Sets the factor

                Console.CursorVisible = false; // Hide cursor
                Console.Write("- "); // New line
            }
        }

        // Displays all settings
        public void DisplaySettings()
        {
            // Displays all settings
            Console.Write("\nColorize text: " + colorize + ", Fancy colors: " + fancyColor + ", Column skipping: " + columnSkip + "\nText Color: ");

            // Display text color
            if (!colorize) Console.Write("<Text not colorized>");
            else
                switch (textColor)
                {
                    case TxtColor.RED:
                        Console.Write("Red");
                        break;

                    case TxtColor.YELLOW:
                        Console.Write("Yellow");
                        break;

                    case TxtColor.GREEN:
                        Console.Write("Green");
                        break;

                    case TxtColor.CYAN:
                        Console.Write("Cyan");
                        break;

                    case TxtColor.BLUE:
                        Console.Write("Blue");
                        break;

                    case TxtColor.MAGENTA:
                        Console.Write("Magenta");
                        break;
                }

            //Write setting
            Console.Write(", Fancy-Color-Factor: " + fancyColorFactor + ", Blank-Factor: " + blankSpace + ", \nColumn-Skip-Factor: " + columnSkipFactor);
            Console.Write("\n- "); // New line
        }

        //Sets the factor for blank fields
        public void SetBlankSpaceFactor(int factor)
        {
            blankSpace = factor;
        }

        //Sets the factor for fancy colors
        void SetFancyColorFactor(int factor)
        {
            fancyColorFactor = factor;
        }

        //Sets the factor for column skipping
        void SetColumnSkipFactor(int factor)
        {
            columnSkipFactor = factor;
        }
    }
}
